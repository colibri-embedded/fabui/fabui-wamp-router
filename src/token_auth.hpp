/**
 * Copyright (C) 2018 Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file: token_auth.cpp
 * @brief Token authentication provider
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 */
#ifndef TOKEN_PROVIDER_HPP
#define TOKEN_PROVIDER_HPP

#include <chrono>
#include <fabui/wamp/auth.hpp>

class TokenAuth : public fabui::WampAuth {
	public:
		TokenAuth(const std::string& key = "", const std::string& alg = "hs256");

		wampcc::auth_provider provider();

		wampcc::auth_provider::authenticated onAuthenticate(const std::string& realm, 
				const std::string& authid,
                const std::string& authmethod,
                const std::string& ticket);

		wampcc::auth_provider::authorized onAuthorize(const std::string& realm, 
			const std::string& authrole, 
			const std::string& uri, wampcc::auth_provider::action);

		void setKey(const std::string& key);

		wampcc::json_object generateToken(unsigned sub, 
				const std::string& role, 
				std::chrono::seconds duration = std::chrono::minutes{15});

	private:
		std::string m_key;
		std::string m_alg;
};

#endif /* TOKEN_PROVIDER_HPP */