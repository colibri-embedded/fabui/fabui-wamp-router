/**
 * Copyright (C) 2018 Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file: router.hpp
 * @brief FABUI WAMP application class header
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 */
#ifndef ROUTER_HPP
#define ROUTER_HPP

#include <fabui/wamp/router.hpp>
#include "token_auth.hpp"
#include "backend_client.hpp"

class Router: public fabui::WampRouter {
	public:
		Router(	const std::string& host, 
				unsigned port, 
				const std::string& realm,
				const std::string& api_url,
				const std::string& env_file);
		/**
		 * Run application.
		 * 
		 * @param host	The host address to server the application on
		 * @param port	The port to server the application on
		 * @param realm	Default WAMP realm
		 * @returns 	0 on success, 1 otherwise
		 */
		int run();
		bool listen();
		void terminate(wampcc::wamp_session &caller, wampcc::call_info info);

		void getBackendToken(wampcc::wamp_session &caller, wampcc::call_info info);
		void getBackendUrl(wampcc::wamp_session &caller, wampcc::call_info info);

		void ping(wampcc::wamp_session &caller, wampcc::call_info info);

		void onJoin();
		void onDisconnect();
	private:
		TokenAuth m_token_auth;
		BackendClient m_backend;

		unsigned m_port;
		std::string m_api_url;
		std::string m_host;
		std::string m_realm;
};

#endif /* ROUTER_HPP */