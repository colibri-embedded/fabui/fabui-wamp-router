/**
 * Copyright (C) 2018 Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file: router.cpp
 * @brief FABUI WAMP router class implementation
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 */
#include "router.hpp"
#include <fabui/parser/env.hpp>

using namespace fabui;
using namespace wampcc;

Router::Router(	const std::string& host, unsigned port, const std::string& realm, const std::string& api_url,
				const std::string& env_file) 
	: WampRouter(port)
	, m_backend(api_url)
	, m_api_url(api_url)
	, m_port(port)
	, m_host(host)
	, m_realm(realm)
{

	try {
		auto env = parse_env_file(env_file);

		auto value = env.find("JWT_SECRET");
		if(value != env.end())
			m_token_auth.setKey(value->second);

		auto result = m_token_auth.generateToken(0, "BACKEND");
		std::cout << result << std::endl;

	} catch(const std::exception& e) {
		std::cout << e.what() << std::endl;
	}
}

void Router::terminate(wamp_session &caller, call_info info) {
	std::cout << "terminated\n";
	caller.result(info.request_id);
	disconnect();
}

void Router::getBackendToken(wampcc::wamp_session &caller, wampcc::call_info info) {
	auto result = m_token_auth.generateToken(0, "BACKEND");
	caller.result(info.request_id, {}, result);
}

void Router::getBackendUrl(wampcc::wamp_session &caller, wampcc::call_info info) {
	caller.result(info.request_id, {m_api_url});
}

void Router::onJoin() {
	using namespace std::placeholders;

	callable(m_realm, "wamp.service.terminate", std::bind(&Router::terminate, this, _1, _2));
	
	callable(m_realm, "backend.get_token", std::bind(&Router::getBackendToken, this, _1, _2));
	callable(m_realm, "backend.get_url", std::bind(&Router::getBackendUrl, this, _1, _2));

	callable(m_realm, "wamp.service.ping", std::bind(&Router::ping, this, _1, _2));
}

void Router::ping(wampcc::wamp_session &caller, wampcc::call_info info) {
	caller.result(info.request_id, {"pong"});
}

void Router::onDisconnect() {
	std::cout << "Disconnecting...\n";
}

bool Router::listen() {
	auth_provider auth = m_token_auth.provider();
	return WampRouter::listen(auth);
}

int Router::run() {
	if( listen() ) {
		auto finished = finished_future();
		finished.wait();
		return 0;
	}
	return 1;
}