set(SOURCES
	"${PROJECT_SOURCE_DIR}/src/main.cpp"
	"${PROJECT_SOURCE_DIR}/src/router.cpp"
	"${PROJECT_SOURCE_DIR}/src/token_auth.cpp"
	"${PROJECT_SOURCE_DIR}/src/backend_client.cpp"
	)

add_executable(fabui-wamp-router ${SOURCES})
set_property(TARGET fabui-wamp-router PROPERTY CXX_STANDARD 14)
set_property(TARGET fabui-wamp-router PROPERTY CXX_STANDARD_REQUIRED ON)

target_include_directories(fabui-wamp-router PUBLIC ${3RDPARTY_SOURCE_DIR})

target_link_libraries (fabui-wamp-router ${LIBFABUI_LIBRARIES})

##
## Install targets
##
install (TARGETS fabui-wamp-router DESTINATION "${INSTALL_BIN_DIR}")

# add_subdirectory(plugins)