/**
 * Copyright (C) 2018 Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file: backend.hpp
 * @brief FABUI HTTP backend client
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 */

#ifndef BACKEND_CLIENT_HPP
#define BACKEND_CLIENT_HPP

#include <fabui/http/client.hpp>

#include <string>

class BackendClient {
	public:
		BackendClient(const std::string& base_url);
};

#endif /* BACKEND_HPP */