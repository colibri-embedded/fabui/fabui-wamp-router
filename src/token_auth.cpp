/**
 * Copyright (C) 2018 Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file: token_auth.cpp
 * @brief Token authentication provider
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 */
#include "token_auth.hpp"
#include "jwt/jwt.hpp"
#include <chrono>

using namespace fabui;
using namespace wampcc;

TokenAuth::TokenAuth(const std::string& key, const std::string& alg) 
	: WampAuth("token")
	, m_key(key)
	, m_alg(alg)
{

}

void TokenAuth::setKey(const std::string& key) {
	m_key = key;
}

wampcc::json_object TokenAuth::generateToken(unsigned sub, const std::string& role, std::chrono::seconds expires_in) {
	using namespace jwt::params;

	auto iat_tp = std::chrono::system_clock::now();
	auto iat = std::chrono::duration_cast<std::chrono::seconds>(iat_tp.time_since_epoch()).count();

	auto exp_tp = iat_tp + expires_in;
	auto exp = std::chrono::duration_cast<std::chrono::seconds>(exp_tp.time_since_epoch()).count();

	jwt::jwt_object obj{algorithm(m_alg), secret(m_key), payload({
		{"rle", role}
	})};

    obj.add_claim("iss", "fabui-jwt")
       .add_claim("sub", sub)
       .add_claim("iat", iat)
       .add_claim("exp", exp)
       ;

    std::string token = obj.signature();

	return {
		{"access_token", token},
		{"issued_at", iat},
		{"expires_at", exp}
	};
}

auth_provider TokenAuth::provider() {
	auto auth = auth_provider {
		// provider_name
		[this](const std::string&){
			return m_provider_name;
		},
		// policy
		[this](const std::string& authid, const std::string& realm) {
			return auth_provider::auth_plan(auth_provider::mode::authenticate, {"token"} );
		},
		// cra_salt
		nullptr,
		// cra_check
		nullptr,
		// user_sercer
		nullptr,
		// user_role
		nullptr,
		// authorize
		[this](	const std::string& realm,
                const std::string& authrole,
                const std::string& uri,
                auth_provider::action action){
			return onAuthorize(realm, authrole, uri, action);
		},
		// authenticate
		[this](	const std::string& authid, // not an error, it's authid, realm in wampcc
                const std::string& realm,
                const std::string& authmethod,
                const std::string& signiture){
			return onAuthenticate(realm, authid, authmethod, signiture);
		},
		// hello
		[this](	const std::string& authid,
        		const std::string& realm,
        		const std::string& authmethod,
        		const std::string& authprovider,
        		t_session_id session){
			return json_object();
		}
        
	};

	return std::move(auth);
}

auth_provider::authenticated TokenAuth::onAuthenticate(const std::string& realm, 
		const std::string& authid,
		const std::string& authmethod,
		const std::string& token) 
{
	using namespace jwt::params;
	//std::cout << "Authenticate: (" << authid << ", " << token << ") on " << realm << std::endl;

	try {
		auto dec_obj = jwt::decode(token, algorithms({m_alg}), secret(m_key), verify(true));
		std::string role = "unauthorized";
		std::string new_authid = authid;

		if( dec_obj.payload().has_claim("sub") ) {
			new_authid = std::to_string( dec_obj.payload().get_claim_value<unsigned>("sub") );
		} else {
			std::cout << "no sub\n";
			return {false};
		}

		if( dec_obj.payload().has_claim("rle") ) {
			role = dec_obj.payload().get_claim_value<std::string>("rle");
		} else {
			std::cout << "no rle\n";
			return {false};
		}

		std::cout << "Authenticated: ( authid -> " << new_authid << ", role ->" << role << ")" << std::endl;

		return {
			true,
			role,
			new_authid
		};
	//} catch (const jwt::TokenExpiredError& e) {
		//Handle Token expired exception here
		//...
	//} catch (const jwt::SignatureFormatError& e) {
		//Handle invalid signature format error
		//...
	//} catch (const jwt::DecodeError& e) {
		//Handle all kinds of other decode errors
		//... 
	//} catch (const jwt::VerificationError& e) {
		// Handle the base verification error.
		//NOTE: There are other derived types of verification errors
		// which will be discussed in next topic.
	} catch (const std::exception& e) {
		std::cout << "Authentication error: " << e.what() << std::endl;
	}

	return {false};
}

auth_provider::authorized TokenAuth::onAuthorize(const std::string& realm, 
	const std::string& authrole, 
	const std::string& uri, auth_provider::action) 
{
	return {true, auth_provider::disclosure::always};
}