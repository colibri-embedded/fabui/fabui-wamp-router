/**
 * Copyright (C) 2018 Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @file: main.cpp
 * @brief FABUI WAMP compatible router main
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 */
#include "config.h"
#include <CLI/CLI.hpp>

#include "router.hpp"

using namespace std;

void show_version(size_t) {
  std::cout << PACKAGE_STRING << std::endl; 
  throw CLI::Success();
}

int main(int argc, char** argv) {
	
	CLI::App appargs{"FABUI WAMP router"};

	unsigned	port = 9001;
	string  	host = "0.0.0.0";
	string 		default_realm = "fabui";
	string 		api_uri = "http://127.0.0.1:8000";
	string 		env_file = "/mnt/projects/Colibri-Embedded/fabui/fabui-frontend/application/.env";

	appargs.add_option("-P,--port", port, "WAMP port [default: "+std::to_string(port)+"]");
	appargs.add_option("-H,--host", host, "WAMP host address to serve the router on. [default "+host+"]");
	appargs.add_option("-R,--realm", default_realm, "WAMP default realm [default: "+default_realm+"]");
	appargs.add_option("-B,--backend", api_uri, "FABUI web api [default: " + api_uri + "]");
	appargs.add_option("-e,--backend-env", env_file, "FABUI web .env file [default: " + env_file + "]");
	appargs.add_config("-c,--config", "config.ini", "Read config from an ini file", false);
	appargs.add_flag_function("-v,--version", show_version, "Show version");

	try {
		appargs.parse(argc, argv);
	} catch (const CLI::ParseError &e) {
		return appargs.exit(e);
	}

	Router router(host, port, default_realm, api_uri, env_file);
	return router.run();
}